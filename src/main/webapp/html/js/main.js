function getURLParams()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }

    if (!("lang" in vars)) {
      vars["lang"] = "EN";
    }

    if (!("pilotId" in vars)) {
      showModal(getI18N('EN', 'error-title'), "pilotId not specified in URL params", returnError, getI18N('EN', 'accept-button'));
    }

    if (!("app" in vars)) {
      showModal(getI18N('EN', 'error-title'), "app not specified in URL params", returnError, getI18N('EN', 'accept-button'));
    }

    if (!("callback" in vars)) {
      showModal(getI18N('EN', 'error-title'), "callback not specified in URL params", returnError, getI18N('EN', 'accept-button'));
    }

    return vars;
}

function getBaseURL() {
  var index = document.URL.indexOf('html');
  var baseURL = document.URL.substring(0, index);

  return baseURL;
}

function createQuestions() {
  var baseURL = getBaseURL();
  var urlParams = getURLParams();

  var questionSetURL = baseURL +
    'api/questionnaire/question-set?lang=' + urlParams['lang'];

  var lang = urlParams['lang'];

  $("#submit-button").val(getI18N(lang, 'submit'));
  $("#cancel-button").val(getI18N(lang, 'cancel'));

  $.ajax({
    url: questionSetURL,
    success: function(data) {
      for (var i = 0; i < data.questions.length; i++) {
        var question = data.questions[i];
        createQuestionElement(i, question);
      }

      $("#question-set-id").val(data["id"]);

      $("#loading").css('display', 'none');
      $("#questions-area").css('display', 'block');
    },
    error: function(){
      showModal(getI18N(lang, 'error-title'), getI18N(lang, 'get-error'), returnError, getI18N(lang, 'accept-button'));
    }
  });
}

function addOptions(id, values, labels) {
  for (var i = 0; i < values.length; i++) {
    var radio = '' +
      '<label class="radio-inline response">' +
        '<input type="radio" name="options-input-' + id + '" value="' + values[i] + '">' +  labels[i] +
      '</label>';

    $("#options-input-" + id).append(radio);
  }
}

function createQuestionElement(id, question) {
  if (question.type == 'RANGE_SET') {
    question_element = '' +
      '<div class="row gray">' +
        '<div class="col-md-8 text question">' + question.text + '</div>' +
        '<div class="col-md-4 small-star question"><input name="' + question.id + '" id="star-input-' + id + '" type="text" class="rating"></div>' +
      '</div>';

    $("#questions").append(question_element);
    eval('var values =' + question.allowedResponse);
    var hasClear = values.min == 0;
    var starOptions = {
        showCaption: false,
        showClear: hasClear,
        min: 0,
        max: values.max,
        stars: values.num,
        step: values.step,
        hoverEnabled: false
    };

    $("#star-input-" + id).rating(starOptions);
    $("#star-input-" + id).rating('update', values.min);

  } else if (question.type == 'FREE_TEXT') {
    question_element = '' +
      '<div class="row gray">' +
        '<div class="col-md-8 text question">' + question.text + '</div>' +
        '<div class="col-md-4 text question"><textarea name="' + question.id + '" id="text-input-' + id + '" class="form-control" rows="1"></textarea></div>' +
      '</div>';

    $("#questions").append(question_element);
  } else if (question.type == 'VALUE_SET') {
    question_element = '' +
      '<div class="row gray">' +
        '<div class="col-md-8 text question">' + question.text + '</div>' +
        '<div class="col-md-4 text question"><div name="' + question.id + '" id="options-input-' + id + '"></div>' +
      '</div>';

    $("#questions").append(question_element);
    eval('var values =' + question.allowedResponse);
    eval('var labels =' + question.responseLabels);
    addOptions(id, values, labels);
  }
}

function processValueSets() {
  var responses = [];
  var valueSets = $('[id^="options-"]').each(function (index, value) {
    var question = $(this).attr('name');
    var value = $('input[name=' + $(this).attr('id') + ']:checked').val();

    var response = {
      "questionId": parseInt(question),
      "response": "",
    };

    if (value != undefined) {
      response.response = value;
    }

    responses.push(response);
  });

  return responses;
}

function processRangeSets() {
  var responses = [];
  var starInputs = $('[id^="star-input-"]').each(function (index, value) {
    var question = $(this).attr('name');
    var value = $(this).val();
    var response = {
      "questionId": parseInt(question),
      "response": value,
    }

    responses.push(response);
  });

  return responses;
}

function processFreeTexts() {
  var responses = [];
  var valueSets = $('[id^="text-input-"]').each(function (index, value) {
    var question = $(this).attr('name');
    var value = $(this).val();

    var response = {
      "questionId": parseInt(question),
      "response": value,
    };

    responses.push(response);
  });

  return responses;
}

function returnOk() {
  var urlParams = getURLParams();
  window.location.replace(urlParams['callback'] + '?questionnaire-status=OK');
}

function returnError() {
  var urlParams = getURLParams();
  window.location.replace(urlParams['callback'] + '?questionnaire-status=ERROR');
}

function returnCancel() {
  var urlParams = getURLParams();
  window.location.replace(urlParams['callback'] + '?questionnaire-status=CANCEL');
}

function showModal(title, message, callback, button) {
  $('#modal-title').text(title);
  $('#modal-body').html('<h4>' + message + '</h4>');

  $('#dismiss-button').click(callback);
  $('#ok-button').text(button);
  $('#ok-button').click(callback);

  $('#myModal').modal();
}

function processResponses() {
  var responses = [];

  var valueSetResponses = processValueSets();
  Array.prototype.push.apply(responses, valueSetResponses);

  var rangeSetResponses = processRangeSets();
  Array.prototype.push.apply(responses, rangeSetResponses);

  var freeTextResponses = processFreeTexts();
  Array.prototype.push.apply(responses, freeTextResponses);

  var urlParams = getURLParams();

  var responseData = {
    "questionSetId": parseInt($("#question-set-id").val()),
    "lang": urlParams["lang"],
    "pilotId": decodeURIComponent(urlParams["pilotId"]),
    "app": decodeURIComponent(urlParams["app"]),
    "email": "",
    "responses" : responses
  };

  var baseURL = getBaseURL();
  var postURL = baseURL + 'api/questionnaire/response-set'

  var lang = urlParams['lang'];

  $.ajax({
    type: "POST",
    url: postURL,
    data: JSON.stringify(responseData),
    contentType: "application/json",
    success: function() {
      showModal(getI18N(lang, 'ok-title'), getI18N(lang, 'ok-message'), returnOk, getI18N(lang, 'accept-button'));
    },
    error: function(data){
      showModal(getI18N(lang, 'error-title'), getI18N(lang, 'post-error'), returnError, getI18N(lang, 'accept-button'));
    }
  });
}

function getI18N(lang, name) {
  var translations = {
    'EN': {
      'submit': 'Submit',
      'cancel': 'Cancel',
      'error-title': 'Error',
      'get-error': 'Could not obtain questions from server',
      'post-error': 'Could not send response to server',
      'ok-title': 'Thank you!',
      'ok-message': 'Your responses are very valuable to us',
      'accept-button': 'Accept'
    },
    'ES': {
      'submit': 'Enviar',
      'cancel': 'Cancelar',
      'error-title': 'Error',
      'get-error': 'No se pudieron obtener las preguntas del servidor',
      'post-error': 'No se pudieron enviar las respuestas al servidor',
      'ok-title': '¡Muchas gracias!',
      'ok-message': 'Tus respuestas son muy importantes para nosotros',
      'accept-button': 'Aceptar'
    },
    'IT': {
      'submit': 'Invia',
      'cancel': 'Annulla',
      'error-title': 'Errore',
      'get-error': 'Domande non disponibili dal server',
      'post-error': 'Non è stato possibile inviare le risposte al server',
      'ok-title': 'Grazie!',
      'ok-message': 'Le tue risposte sono molto importanti per noi',
      'accept-button': 'Acconsento'
    },
    'SR': {
      'submit': 'Pošalji',
      'cancel': 'Odustani',
      'error-title': 'Greška',
      'get-error': 'Ne mogu da se dobiju pitanja sa servera',
      'post-error': 'Ne može se poslati odziv serveru',
      'ok-title': 'Hvala Vam!',
      'ok-message': 'Vaši odzivi su nam veoma značajni',
      'accept-button': 'Prihvati'
    },
    'FI': {
      'submit': 'Lähetä',
      'cancel': 'Peruuta',
      'error-title': 'Virhe',
      'get-error': 'Kysymyksiä ei saatu palvelimelta',
      'post-error': 'Vastausta ei voitu lähettää',
      'ok-title': 'Kiitos!',
      'ok-message': 'Sinun vastauksesi ovat arvokkaita meille',
      'accept-button': 'Hyväksytty'
    }
  };

  if (!(lang in translations)) {
    return translations['EN'][name];
  } else {
    return translations[lang][name];
  }
}

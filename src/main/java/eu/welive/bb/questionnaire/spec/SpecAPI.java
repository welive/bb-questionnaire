/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.spec;

import java.io.File;
import java.io.IOException;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.commons.io.FileUtils;

@Path("/spec")
public class SpecAPI {

	@GET
    @Path("/list")
	public String getList() {
		return "asdasdsad";
	}
	
	@GET
    @Path("/xwadl")
	@Produces(MediaType.TEXT_PLAIN)
	public String getXWadl() throws IOException {
		final ClassLoader classLoader = getClass().getClassLoader();
		final File wadlFile = new File(classLoader.getResource("data/welive.wadl").getFile());
		final String data = FileUtils.readFileToString(wadlFile, "UTF-8");
		System.out.println(data);
		return data;
	}
	
	@GET
    @Path("/usdl")
	public String getUSDL() {
		return "asdasdsad";
	}
}

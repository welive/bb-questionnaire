/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.logging;

import java.net.URISyntaxException;
import java.util.Date;

import org.json.JSONObject;

import eu.welive.bb.questionnaire.serialization.Response;
import eu.welive.bb.questionnaire.serialization.ResponseSet;

public class InAppKPI {
	
	private static final String VALUE = "value";
	private static final String TEXT = "text";
	private static final String CUSTOM_ATTR = "custom_attr";
	private static final String RESPONSE_SET_ID = "responseSetId";
	private static final String PILOT = "pilot";
	private static final String APP_NAME = "appname";
	private static final String KPI = "kpi";
	private static final String MSG = "msg";
	private static final String TIMESTAMP = "timestamp";
	private static final String TYPE = "type";
	private static final String APP_ID = "appId";
	
	private static final int QUESTION_SET_ID = 1;

	public enum KpiNames {
		KPI1A("KPI1.A"),
		KPI1B("KPI1.B"),
		KPI2("KPI2"),
		KPI3("KPI3"),
		KPI4A("KPI4A"),
		KPI4B("KPI4B"),
		KPI4C("KPI4C"),
		KPI4D("KPI4D");
		
		private final String name;
		
		private KpiNames(String name) {
			this.name = name;
		}
		
		@Override
		public String toString() {
			return name;
		}
	}
	
	private static final String LOGGING_SERVICE_NAME = "in-app";
	private static final String LOGGING_TYPE = "QuestionnaireFilled";
	
	private final LoggingService loggingService;
	
	public InAppKPI(String api, String token) {
		loggingService = new LoggingService(api, token);
	}
	
	public void updateKPI(ResponseSet responseSet) throws URISyntaxException {
		if (responseSet.getQuestionSetId() == QUESTION_SET_ID) {
			logData(KpiNames.KPI1A, responseSet.getApp(), responseSet.getPilotId(), responseSet.getId());
			
			for (final Response response : responseSet.getResponses()) {
				switch (response.getQuestionId()) {
					case 7:		if (!response.getResponse().isEmpty()) {
									logData(KpiNames.KPI1B, responseSet.getApp(), responseSet.getPilotId(), responseSet.getId(), response.getResponse());
								}
								break;
								
					case 6:		if (!response.getResponse().isEmpty()) {
									logData(KpiNames.KPI2, responseSet.getApp(), responseSet.getPilotId(), responseSet.getId(), response.getResponse());
								}
								break;
								
					case 4:		logData(KpiNames.KPI3, responseSet.getApp(), responseSet.getPilotId(), responseSet.getId(), Integer.parseInt(response.getResponse()));
								break;
								
					case 2:		logData(KpiNames.KPI4A, responseSet.getApp(), responseSet.getPilotId(), responseSet.getId(), Integer.parseInt(response.getResponse()));
								break;
								
					case 3:		logData(KpiNames.KPI4B, responseSet.getApp(), responseSet.getPilotId(), responseSet.getId(), Integer.parseInt(response.getResponse()));
								break;
					
					case 1:		logData(KpiNames.KPI4C, responseSet.getApp(), responseSet.getPilotId(), responseSet.getId(), Integer.parseInt(response.getResponse()));
								break;
								
					case 5:		logData(KpiNames.KPI4D, responseSet.getApp(), responseSet.getPilotId(), responseSet.getId(), Integer.parseInt(response.getResponse()));
								break;
				}
			}			
		}
	}
	
	private void logData(KpiNames kpi, String app, String pilot, Integer responseSetId) throws URISyntaxException {
		final JSONObject payload = createPayload(kpi);
		
		final JSONObject customAttributes = createCommonAttr(kpi, app, pilot, responseSetId);
		payload.put(CUSTOM_ATTR, customAttributes);
		
		loggingService.sendLog(LOGGING_SERVICE_NAME, payload);
	}

	private JSONObject createCommonAttr(KpiNames kpi, String app, String pilot, Integer responseSetId) {
		final JSONObject customAttributes = new JSONObject();
		customAttributes.put(KPI, kpi);
		customAttributes.put(APP_NAME, app);
		customAttributes.put(PILOT, pilot);
		customAttributes.put(RESPONSE_SET_ID, responseSetId);
		return customAttributes;
	}
	
	private void logData(KpiNames kpi, String app, String pilot, Integer responseSetId, String text) throws URISyntaxException {
		final JSONObject payload = createPayload(kpi);
		
		final JSONObject customAttributes = createCommonAttr(kpi, app, pilot, responseSetId);
		customAttributes.put(TEXT, text);
		payload.put(CUSTOM_ATTR, customAttributes);
		
		loggingService.sendLog(LOGGING_SERVICE_NAME, payload);
	}
	
	private void logData(KpiNames kpi, String app, String pilot, Integer responseSetId, Integer value) throws URISyntaxException {
		final JSONObject payload = createPayload(kpi);
		
		final JSONObject customAttributes = createCommonAttr(kpi, app, pilot, responseSetId);
		customAttributes.put(VALUE, value);
		payload.put(CUSTOM_ATTR, customAttributes);
		
		loggingService.sendLog(LOGGING_SERVICE_NAME, payload);
	}

	private JSONObject createPayload(KpiNames kpi) {
		final JSONObject payload = new JSONObject();
		payload.put(APP_ID, LOGGING_SERVICE_NAME);
		payload.put(TYPE, LOGGING_TYPE);
		payload.put(TIMESTAMP, new Date().getTime() / 1000);
		payload.put(MSG, LOGGING_TYPE + " " + kpi);
		return payload;
	}
}
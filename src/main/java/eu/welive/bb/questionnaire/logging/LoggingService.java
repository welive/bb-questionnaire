/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.logging;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;

import org.json.JSONObject;

public class LoggingService {
	 
	private final String token;
	private final WebTarget target;
	
	public LoggingService(String api, String token) {
		this.token = token;
		
		final Client client = ClientBuilder.newClient();
		target = client.target(api);
	}

	public void sendLog(String appId, JSONObject payload) {
		final Entity<String> data = Entity.entity(payload.toString(), MediaType.APPLICATION_JSON);
		
		target.path("log/" + appId)
			.request()
			.header("Authorization", token)
			.post(data);
	}
}

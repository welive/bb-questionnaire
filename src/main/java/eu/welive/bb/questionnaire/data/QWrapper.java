/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.data;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.json.JsonObject;
import javax.json.JsonValue;

import org.apache.log4j.Logger;

import eu.welive.bb.questionnaire.data.qm.QMConnector;
import eu.welive.bb.questionnaire.data.qm.QMConnectorException;
import eu.welive.bb.questionnaire.logging.InAppKPI;
import eu.welive.bb.questionnaire.serialization.Question;
import eu.welive.bb.questionnaire.serialization.Question.Language;
import eu.welive.bb.questionnaire.serialization.Question.QuestionType;
import eu.welive.bb.questionnaire.serialization.QuestionSet;
import eu.welive.bb.questionnaire.serialization.Response;
import eu.welive.bb.questionnaire.serialization.ResponseSet;
import eu.welive.bb.questionnaire.serialization.survey.Survey;
import eu.welive.bb.questionnaire.serialization.survey.SurveySet;

public class QWrapper {
	
	public static final String ALL_PILOTS = "All";
	
	final static Logger logger = Logger.getLogger(QWrapper.class);
	
	private final QMConnector qmConnector;
	
	private final String dataset;
	private final String resource;
	private final String token;
	
	public static final int LATEST_QUESTIONSET = -1;
	
	public QWrapper(String dataset, String resource, QMConnector qmConnector, String token) throws QWrapperException {
		this.dataset = dataset;
		this.resource = resource;
		this.qmConnector = qmConnector;
		this.token = token;
	}
	
	private String escapeSQL(String str) {
		return str.replace("'", "''");
	}
	
	public int addResponseSet(ResponseSet responseSet) throws QWrapperException {
		logger.debug("Adding ResponseSet for app " + responseSet.getApp() + ", QuestionSet ID: " + responseSet.getQuestionSetId());
		
		checkResponseSet(responseSet);
		
		try {
			
			final String insertRSTemplate = "INSERT INTO ResponseSet (id, questionset_id, app, lang, country, timestamp, email) VALUES (null, %d, \"%s\", \"%s\", \"%s\", \"%s\", \"%s\")";

			final String insertResponseSet = String.format(insertRSTemplate, responseSet.getQuestionSetId(), responseSet.getApp(), responseSet.getLang(), responseSet.getPilotId(), qmConnector.createTimestamp(), responseSet.getEmail());
					
			final List<String> inserts = new ArrayList<String>();
			inserts.add(insertResponseSet);
			
			final String insertResponseTemplate = "INSERT INTO replies (id, value, responseset_id, question_id) SELECT null, '%s', MAX(id), %d FROM ResponseSet";		
			for (final Response r : responseSet.getResponses()) {
				final String insertResponse = String.format(insertResponseTemplate, escapeSQL(r.getResponse()), r.getQuestionId());
				inserts.add(insertResponse);
			}
			
			logger.debug("Executing update: ");
			logger.debug(inserts);
			
			
			int responseSetId = -1;
			final String selectResponseSetID = "SELECT MAX(id) as responseSetId FROM ResponseSet";
			final JsonObject jsonResponse = qmConnector.queryDataset(selectResponseSetID, dataset, resource);
			if (jsonResponse.containsKey("rows")) {
				final JsonObject jsonObject = (JsonObject) jsonResponse.getJsonArray("rows").get(0);
				responseSetId = jsonObject.getInt("responseSetId");
			}
			
			final int updatedRows = qmConnector.updateDatasetTransactional(inserts, dataset, resource);
			
			responseSet.setId(responseSetId);
			
			final InAppKPI updater = new InAppKPI(qmConnector.getAPI(), token);
			updater.updateKPI(responseSet);
			
			return updatedRows;
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		} catch (URISyntaxException e) {
			throw new QWrapperException(e.getMessage());
		}
	}

	private void checkResponseSet(ResponseSet responseSet) throws QWrapperException {
		if (responseSet.getQuestionSetId() == -1) {
			final String msg = "ResponseSet has no 'questionSetId'";
			logger.error(msg);
    		throw new QWrapperException(msg);
    	}
    	
    	if (responseSet.getLang() == null) {
    		final String msg = "ResponseSet has no 'lang'";
    		logger.error(msg);
    		throw new QWrapperException(msg);
    	}
    	
    	if (responseSet.getPilotId() == null) {
    		final String msg = "ResponseSet has no 'pilotID'";
    		logger.error(msg);
    		throw new QWrapperException(msg);
    	}
    	
    	final QuestionSet questionSet = getQuestionSet(responseSet.getQuestionSetId(), responseSet.getLang());
    	
    	final Set<Integer> questionIds = new HashSet<Integer>();
    	for (Question question : questionSet.getQuestions()) {
    		questionIds.add(question.getId());
    	}
    	
    	for (Response response : responseSet.getResponses()) {
    		if (!questionIds.contains(response.getQuestionId())) {
    			final String msg = String.format("Question '%d' not found in QuestionSet '%d'", response.getQuestionId(), questionSet.getId());
    			logger.error(msg);
    			throw new QWrapperException(msg);
    		} else {
    			questionIds.remove(response.getQuestionId());
    		}
    	}
    	
    	if (!questionIds.isEmpty()) {
    		final String msg = String.format("Some questions of QuestionSet '%d' were not answered: %s", questionSet.getId(), questionIds);
    		logger.error(msg);
    		throw new QWrapperException(msg);
    	}
	}

	public QuestionSet getQuestionSet(int id, Language lang) throws QWrapperException {
		logger.debug("Obtaining QuestionSet with lang: " + lang + " id: " + id);
		
		String queryTemplate = "SELECT QuestionSet.id as questionset_id, title, description, app, " + 
				"QuestionSet.timestamp, Question.id as id, text, QTranslation.lang, \"order\", type, allowed, labels " +
		        "FROM QuestionSet, contains, Question, QuestionType, QTranslation " + 
		        "LEFT JOIN LTranslation ON (QuestionType.id = LTranslation.questiontype_id AND LTranslation.lang = '%s') " + 
		        "WHERE contains.questionset_id = QuestionSet.id " +
		            "AND contains.question_id = Question.id " +
		            "AND QuestionType.id = Question.type_id " +
		            "AND QTranslation.question_id = Question.id " +
		            "AND (QTranslation.lang = '%s' OR QTranslation.lang = 'EN')";
		            
		if (id != LATEST_QUESTIONSET) {
			queryTemplate += " AND QuestionSet.id = %d";  
		}
		 
		queryTemplate += " GROUP BY Question.id ORDER BY \"order\"";
		
		String query = "";
		if (id != LATEST_QUESTIONSET) {
			query = String.format(queryTemplate, lang, lang, id);
		} else {
			query = String.format(queryTemplate, lang, lang);
		}
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		
		try {
			final QuestionSet response = processQuestionQuery(query);
			if (response.getId() != -1) {
				return response;
			}
			else {
				String msg = "";
				if (id != LATEST_QUESTIONSET) {
					msg = String.format("QuestionSet '%d' not found", id);
				} else {
					msg = "QuestionSet not found";
				}
				logger.error(msg);
				throw new QWrapperException(msg);
			}
		} catch (QMConnectorException e) {
			logger.error(e.getMessage());
			throw new QWrapperException(e);
		}
	}
	
	public List<QuestionSet> getQuestionSets() throws QWrapperException {
		logger.debug("Obtaining all QuestionSets");
		
		final String query = "SELECT QuestionSet.id as questionset_id, title, description, app, " + 
				"QuestionSet.timestamp, Question.id as id, text, QTranslation.lang, \"order\", type, allowed, labels " +
		        "FROM QuestionSet, contains, Question, QuestionType, QTranslation " + 
		        "LEFT JOIN LTranslation ON (QuestionType.id = LTranslation.questiontype_id) " + 
		        "WHERE contains.questionset_id = QuestionSet.id " +
		            "AND contains.question_id = Question.id " +
		            "AND QuestionType.id = Question.type_id " +
		            "AND QTranslation.question_id = Question.id " +
		            "AND QTranslation.lang = 'EN'" +
		        "GROUP BY Question.id";
		
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			final List<QuestionSet> questionSets = processQuestionSetsQuery(query);
			return questionSets;
		} catch (QMConnectorException e) {
			logger.error(e.getMessage());
			throw new QWrapperException(e);
		} 
	}
	
	public SurveySet getSurveySet(String authHeader, String pilotId) throws QWrapperException {		
    	final SurveySet surveySet = new SurveySet();
    	final List<QuestionSet> questionSets = getQuestionSets();
    	
    	for (final QuestionSet questionSet : questionSets) {
        	final Survey survey = new Survey();
	    	survey.setQuestionSet(questionSet);
	    	
	    	final List<ResponseSet> responseSets = getResponseSets(authHeader, questionSet.getId(), pilotId);
	    	survey.setResponseSets(responseSets);
    		
    		surveySet.addSurvey(survey);
    	}
    	
    	return surveySet;
	}
	
	private List<ResponseSet> getResponseSets(String authHeader, int questionSetId, String pilotId) throws QWrapperException {
		String queryTemplate = "SELECT ResponseSet.id as responseset_id, questionset_id, email, app, lang, country, " +
				"ResponseSet.timestamp as responseset_timestamp, value, question_id " + 
				"FROM ResponseSet, replies, Question " + 
				"WHERE replies.responseset_id = ResponseSet.id " + 
					"AND Question.id = replies.question_id " + 		     
		            "AND ResponseSet.questionset_id = %d";
		
		String query = "";
		if (!pilotId.equals(ALL_PILOTS)) {
			queryTemplate += " AND ResponseSet.country = '%s' ORDER BY ResponseSet.timestamp DESC";
			query = String.format(queryTemplate, questionSetId, pilotId);
		} else {
			queryTemplate += " ORDER BY ResponseSet.timestamp DESC";
			query = String.format(queryTemplate, questionSetId);
		}
				
		logger.debug("Executing query: ");
		logger.debug(query);
		
		try {
			return processResponseSetsQuery(authHeader, query);
		} catch (QMConnectorException e) {
			throw new QWrapperException(e);
		}
	}
	
	private List<ResponseSet> processResponseSetsQuery(String authHeader, String query) throws QMConnectorException {
		qmConnector.setAuthHeader(authHeader);
		final JsonObject jsonResponse = qmConnector.queryDataset(query, dataset, resource);
		
		if (jsonResponse.containsKey("count")) {
			if (jsonResponse.getInt("count") == 0) {
				return Collections.emptyList();
			}
		} else {
			throw new QMConnectorException("Invalid JSON received. 'count' field not found");
		}
		
		final List<ResponseSet> responseSets = new ArrayList<ResponseSet>();
		final Map<Integer, ResponseSet> responseSetMap = new HashMap<Integer, ResponseSet>();
		
		if (jsonResponse.containsKey("rows")) {
			for (final JsonValue rowValue : jsonResponse.getJsonArray("rows")) {
				final JsonObject rowObj = (JsonObject) rowValue;
		
				final int responseSetId = rowObj.getInt("responseset_id");
				if (!responseSetMap.containsKey(responseSetId)) {
					final ResponseSet responseSet = new ResponseSet();
					responseSetMap.put(responseSetId, responseSet);
					responseSets.add(responseSet);
				}
				
				final ResponseSet responseSet = responseSetMap.get(responseSetId);				
				responseSet.setId(rowObj.getInt("responseset_id"));
				responseSet.setQuestionSetId(rowObj.getInt("questionset_id"));
				responseSet.setApp(rowObj.getString("app"));
				responseSet.setEmail(rowObj.getString("email"));
				responseSet.setLang(Language.valueOf(rowObj.getString("lang")));
				responseSet.setPilotId(rowObj.getString("country"));
				responseSet.setTimestamp(rowObj.getString("responseset_timestamp"));
				
				final Response response = new Response();
				response.setResponse(rowObj.getString("value"));
				response.setQuestionId(rowObj.getInt("question_id"));
				
				responseSet.addResponse(response);
			}
			
			return responseSets;
		} else {
			final String msg = "Invalid JSON received. 'rows' field not found";
			throw new QMConnectorException(msg);
		}
	}
	
	private List<QuestionSet> processQuestionSetsQuery(final String query) throws QMConnectorException {
		final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
		
		if (response.containsKey("count")) {
			if (response.getInt("count") == 0) {
				return Collections.emptyList();
			}
		} else {
			throw new QMConnectorException("Invalid JSON received. 'count' field not found");
		}
		
		if (response.containsKey("rows")) {
			final Map<Integer, QuestionSet> questionSets = new HashMap<Integer, QuestionSet>();
			
			for (final JsonValue rowValue : response.getJsonArray("rows")) {
				final JsonObject rowObj = (JsonObject) rowValue;
				
				final int questionSetId = rowObj.getInt("questionset_id");
				if (!questionSets.containsKey(questionSetId)) {
					final QuestionSet questionSet = new QuestionSet();
					
					questionSet.setId(rowObj.getInt("questionset_id"));
					questionSet.setTitle(rowObj.getString("title"));
					questionSet.setDescription(rowObj.getString("description"));
					questionSet.setApp(rowObj.getString("app"));
					questionSet.setTimestamp(rowObj.getString("timestamp"));
				
					questionSets.put(questionSetId, questionSet);
				}
				
				final QuestionSet questionSet = questionSets.get(questionSetId);
				
				final Question question = new Question();
				question.setId(rowObj.getInt("id"));
				question.setLang(Language.valueOf(rowObj.getString("lang")));
				question.setOrder(rowObj.getInt("order"));
				question.setText(rowObj.getString("text"));
				question.setType(QuestionType.valueOf(rowObj.getString("type")));
				question.setAllowedResponse(rowObj.getString("allowed"));
				
				if (rowObj.containsKey("labels")) {
					question.setResponseLabels(rowObj.getString("labels"));
				}
				
				questionSet.addQuestion(question);
			}
			
			return new ArrayList<QuestionSet>(questionSets.values());
		} else {
			throw new QMConnectorException("Invalid JSON received. 'rows' field not found");
		}
	}

	private QuestionSet processQuestionQuery(final String query) throws QMConnectorException {
		final JsonObject response = qmConnector.queryDataset(query, dataset, resource);
		
		final QuestionSet questionSet = new QuestionSet();
		
		if (response.containsKey("count")) {
			if (response.getInt("count") == 0) {
				return questionSet;
			}
		} else {
			throw new QMConnectorException("Invalid JSON received. 'count' field not found");
		}
		
		if (response.containsKey("rows")) {
			for (final JsonValue rowValue : response.getJsonArray("rows")) {
				final JsonObject rowObj = (JsonObject) rowValue;
				
				questionSet.setId(rowObj.getInt("questionset_id"));
				questionSet.setTitle(rowObj.getString("title"));
				questionSet.setDescription(rowObj.getString("description"));
				questionSet.setApp(rowObj.getString("app"));
				questionSet.setTimestamp(rowObj.getString("timestamp"));
				
				final Question question = new Question();
				question.setId(rowObj.getInt("id"));
				question.setLang(Language.valueOf(rowObj.getString("lang")));
				question.setOrder(rowObj.getInt("order"));
				question.setText(rowObj.getString("text"));
				question.setType(QuestionType.valueOf(rowObj.getString("type")));
				question.setAllowedResponse(rowObj.getString("allowed"));
				
				if (rowObj.containsKey("labels")) {
					question.setResponseLabels(rowObj.getString("labels"));
				}
				
				questionSet.addQuestion(question);
			}
			
			return questionSet;
		} else {
			throw new QMConnectorException("Invalid JSON received. 'rows' field not found");
		}
	}
}

/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.data.qm;

@SuppressWarnings("serial")
public class QMConnectorException extends Exception {
	
	private final ServerError serverError; 

	public QMConnectorException(String message, ServerError errorMessage) {
		super(message);
		this.serverError = errorMessage;
	}
	
	public QMConnectorException(String message) {
		super(message);
		this.serverError = null; 
	}
	
	public ServerError getServerError() {
		return serverError;
	}
}

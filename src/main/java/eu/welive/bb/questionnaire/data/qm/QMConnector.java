/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.data.qm;

import java.io.IOException;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class QMConnector {
	
	static final String AUTH_HEADER = "Authorization";

	private final String api;
	private final WebTarget target;
	
	private String authHeader = "";
	
	public QMConnector(String api) throws IOException {
		this.api = api;
		final Client client = ClientBuilder.newClient();
		target = client.target(api);
	}
	
	public QMConnector(WebTarget target) {
		this.api = "";
		this.target = target;
	}
	
	public String getAPI() {
		return api;
	}
	
	public JsonObject queryDataset(String query, String dataset, String resource) throws QMConnectorException {
		final Entity<String> entity = Entity.entity(query, MediaType.TEXT_PLAIN);
		
		try {
			final String path = String.format("/ods/%s/resource/%s/query", dataset, resource);
			final Response response = post(path, Collections.<String, String> emptyMap(), entity);
			
			if (response.getStatus() == Status.OK.getStatusCode()) {			
				final JsonObject jsonObj = readJsonObject(response);
				return jsonObj;
			} else {
				final ServerError serverError = getServerError(response);
				throw new QMConnectorException(String.format("Problem executing query on dataset '%s' resource '%s'. ", dataset, resource), serverError);
			}
		} catch (ProcessingException e) {
			final String msg = String.format("Problem executing query on dataset '%s' resource '%s'." + e.getMessage(), dataset, resource);
			throw new QMConnectorException(msg);
		}
	}
	
	private ServerError getServerError(Response response) {
		final JsonObject jsonObject = readJsonObject(response);
		return new ServerError(jsonObject.getString("message"), jsonObject.getString("reason"), jsonObject.getInt("status"));
	}

	private Response post(String path, Map<String, String> queryParams, final Entity<String> entity) {
		WebTarget currentTarget = target.path(path);
		for (Entry<String, String> queryParam : queryParams.entrySet()) {
			currentTarget = currentTarget.queryParam(queryParam.getKey(), queryParam.getValue());
		}
		
		Builder builder = currentTarget.request(MediaType.APPLICATION_JSON);
		if (!authHeader.isEmpty()) {
			builder = builder.header(AUTH_HEADER, authHeader);
		}
		final Response response = builder.post(entity);	
		return response;
	}
	
	public int updateDataset(String update, String dataset, String resource) throws QMConnectorException {
		final Entity<String> entity = Entity.entity(update, MediaType.TEXT_PLAIN);
		
		try {
			final String path = String.format("/ods/%s/resource/%s/update", dataset, resource);
			final Response response = post(path, Collections.<String, String> emptyMap(), entity);
			return processUpdateResponse(response, dataset, resource);
		} catch (ProcessingException e) {
			final String msg = String.format("Problem executing update on dataset '%s' resource '%s'. " + e.getMessage(), dataset, resource);
			throw new QMConnectorException(msg);
		}
	}
	
	private String createTransaction(List<String> updates) {
		final StringBuilder strBuilder = new StringBuilder();
		for (final String str : updates) {
			strBuilder.append(str);
			strBuilder.append(";\n");
		}
		
		return strBuilder.toString();
	}
	
	public int updateDatasetTransactional(List<String> updates, String dataset, String resource) throws QMConnectorException {
		final String transaction = createTransaction(updates);
		
		final Entity<String> entity = Entity.entity(transaction, MediaType.TEXT_PLAIN);
		
		try {
			final String path = String.format("/ods/%s/resource/%s/update", dataset, resource);
			final Map<String, String> params = new HashMap<String, String>();
			params.put("transaction", "true");
			final Response response = post(path, params, entity);
			return processUpdateResponse(response, dataset, resource);
		} catch (ProcessingException e) {
			final String msg = String.format("Problem executing update on dataset '%s' resource '%s'. " + e.getMessage(), dataset, resource);
			throw new QMConnectorException(msg);
		}
	}

	private int processUpdateResponse(final Response response, String dataset, String resource) throws QMConnectorException {
		if (response.getStatus() == Status.OK.getStatusCode()) {
			final JsonObject jsonObj = readJsonObject(response);
			
			if (jsonObj.containsKey("rows")) {
				return jsonObj.getInt("rows");
			} else {
				final String msg = String.format("Problem executing update on dataset '%s' resource '%s'. 'rows' field not found in response", dataset, resource);
				throw new QMConnectorException(msg);
			}
		} else {
			final String msg = String.format("Problem executing update on dataset '%s' resource '%s'. " + getServerError(response), dataset, resource);
			throw new QMConnectorException(msg);
		}
	}

	private JsonObject readJsonObject(final Response response) {
		final String responseStr = response.readEntity(String.class);
		final JsonReader jsonReader = Json.createReader(new StringReader(responseStr));
		final JsonObject jsonObj = jsonReader.readObject();
		jsonReader.close();
		return jsonObj;
	}
	
	public String createTimestamp() {
		return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
	}
	
	public void setAuthHeader(String authHeader) {
		this.authHeader = authHeader;
	}
}
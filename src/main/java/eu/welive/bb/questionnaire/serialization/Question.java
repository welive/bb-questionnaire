/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.serialization;

public class Question {

	public enum Language { EN, ES, IT, SR, FI };
	public enum QuestionType { FREE_TEXT, VALUE_SET, RANGE_SET }; 
	
	private int id;
	private int order;
	private String text;
	private Language lang;
	private QuestionType type;
	private String allowedResponse;
	private String responseLabels;
	
	public Question() {
		this.id = -1;
		this.text = "";
		this.allowedResponse = "";
		this.responseLabels = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Language getLang() {
		return lang;
	}

	public void setLang(Language lang) {
		this.lang = lang;
	}

	public QuestionType getType() {
		return type;
	}

	public void setType(QuestionType type) {
		this.type = type;
	}

	public String getAllowedResponse() {
		return allowedResponse;
	}

	public void setAllowedResponse(String allowedResponse) {
		this.allowedResponse = allowedResponse;
	}

	public String getResponseLabels() {
		return responseLabels;
	}

	public void setResponseLabels(String responseLabels) {
		this.responseLabels = responseLabels;
	}
}

/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.serialization;

import java.util.ArrayList;
import java.util.List;

public class PilotIDs {

	private static final List<String> pilotIDs = new ArrayList<String>();
	
	static {
		pilotIDs.add("Bilbao");
		pilotIDs.add("Novi Sad");
		pilotIDs.add("Region on Uusimaa-Helsinki");
		pilotIDs.add("Trento");
	}
	
	public boolean validate(String pilotID) {
		return pilotIDs.contains(pilotID);
	}
}

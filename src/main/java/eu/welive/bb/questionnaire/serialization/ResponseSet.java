/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.serialization;

import java.util.ArrayList;
import java.util.List;

import eu.welive.bb.questionnaire.serialization.Question.Language;

public class ResponseSet {  

	private int questionSetId;
	private int id;
	private String app;
	private Language lang;
	private String pilotId;
	private String email;
	private String timestamp;
	private List<Response> responses; 
	
	public ResponseSet() {
		this.id = -1;
		this.questionSetId = -1;
		this.email = "";
		this.responses = new ArrayList<Response>();
		this.timestamp = "";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(int questionSetId) {
		this.questionSetId = questionSetId;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public Language getLang() {
		return lang;
	}

	public void setLang(Language lang) {
		this.lang = lang;
	}

	public String getPilotId() {
		return pilotId;
	}

	public void setPilotId(String pilotId) {
		this.pilotId = pilotId;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Response> getResponses() {
		return responses;
	}

	public void setResponses(List<Response> responses) {
		this.responses = responses;
	}
	
	public void addResponse(Response response) {
		this.responses.add(response);
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}

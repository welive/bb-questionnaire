/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.serialization.survey;

import java.util.ArrayList;
import java.util.List;

import eu.welive.bb.questionnaire.serialization.QuestionSet;
import eu.welive.bb.questionnaire.serialization.ResponseSet;

public class Survey {

	private QuestionSet questionSet;
	private List<ResponseSet> responseSets;
	
	public Survey() {
		this.responseSets = new ArrayList<ResponseSet>();
	}

	public QuestionSet getQuestionSet() {
		return questionSet;
	}

	public void setQuestionSet(QuestionSet questionSet) {
		this.questionSet = questionSet;
	}

	public List<ResponseSet> getResponseSets() {
		return responseSets;
	}

	public void setResponseSets(List<ResponseSet> responseSets) {
		this.responseSets = responseSets;
	}
}

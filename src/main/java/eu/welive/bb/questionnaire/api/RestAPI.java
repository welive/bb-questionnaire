/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.api;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import eu.welive.bb.questionnaire.config.Config;
import eu.welive.bb.questionnaire.data.QWrapper;
import eu.welive.bb.questionnaire.data.QWrapperException;
import eu.welive.bb.questionnaire.data.qm.QMConnector;
import eu.welive.bb.questionnaire.serialization.MessageResponse;
import eu.welive.bb.questionnaire.serialization.Question;
import eu.welive.bb.questionnaire.serialization.Question.Language;
import eu.welive.bb.questionnaire.serialization.QuestionSet;
import eu.welive.bb.questionnaire.serialization.ResponseSet;
import eu.welive.bb.questionnaire.serialization.survey.Survey;
import eu.welive.bb.questionnaire.serialization.survey.SurveySet;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Path("questionnaire")
@Api(value = "questionnaire")
public class RestAPI {

	private static final String APPLICATION_JSON_UTF8 = MediaType.APPLICATION_JSON + "; charset=utf-8";
	
    @GET
    @Path("/question-set")
    @Produces(APPLICATION_JSON_UTF8) 
    @ApiOperation(value = "Obtains the last question set for the specified language", 
    	response = QuestionSet.class)
    public QuestionSet getLastQuestionSet(
    		@ApiParam(value = "The language of the retrieved questions (EN, ES, IT, FI, SR). Defaults to 'EN'")
    		@QueryParam("lang") @DefaultValue("EN") Language lang,
    		@ApiParam(value = "Optional. Allows to retrieve an specific question set")
    		@QueryParam("id") @DefaultValue("-1") int id) throws QuestionnaireException {
        
    	try {
	    	final QWrapper qWrapper = createQWrapper();
	    	
	    	if (id == - 1) {
	    		final QuestionSet questionSet = qWrapper.getQuestionSet(-1, lang);
	    		return questionSet;
    		} else {
    			final QuestionSet questionSet = qWrapper.getQuestionSet(id, lang);
	    		return questionSet;
    		}
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
		}
    }
    
    @POST
    @Path("/response-set")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation(value = "Adds a new response set", 
		response = MessageResponse.class)
    public MessageResponse addResponseSet(
    		@ApiParam(value = "The response set to add", required = true)
    		ResponseSet responseSet) throws QuestionnaireException {
    	
    	try {
    		final QWrapper qWrapper = createQWrapper();
	    	qWrapper.addResponseSet(responseSet);
	    	
	    	final MessageResponse responseMessage = new MessageResponse();
	    	responseMessage.setMessage("Responses correctly stored");
	    	return responseMessage;
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
		}
    }
    
    
    private QWrapper createQWrapper() throws QuestionnaireException {
    	try {
			final QMConnector qmConnector = new QMConnector(Config.getInstance().getWeLiveAPI());
			
			final QWrapper qWrapper = new QWrapper(Config.getInstance().getDataset(), Config.getInstance().getResource(), qmConnector, Config.getInstance().getAuthToken());
			return qWrapper;
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
    	} catch (IOException e) {
    		throw new QuestionnaireException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Could not connect with questionnaire dataset." + e.getMessage());
    	}
    }

    @GET
    @Path("/get-responses")
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    @ApiOperation(value = "Obtains all the registered responses", 
		response = SurveySet.class)
    public Response getResponse(
    		@HeaderParam("Authorization") @DefaultValue("") String authHeader,
    		@ApiParam(value = "Optional. Allows to retrieve only the responses for a specific pilot")
    		@QueryParam("pilotId") @DefaultValue(QWrapper.ALL_PILOTS) String pilotId) throws QuestionnaireException {
    	
    	try {
    		final QWrapper qWrapper = createQWrapper();
    		final SurveySet surveySet = qWrapper.getSurveySet(authHeader, pilotId);
    		
    		final ByteArrayOutputStream baos = createZip(surveySet);
    		
    		final byte[] zipData = baos.toByteArray();
    		    		    		
    		return Response.ok(zipData, MediaType.APPLICATION_OCTET_STREAM)
    				.header("content-disposition","attachment; filename = responses.zip")
    				.build();
    	} catch (QWrapperException e) {
    		throw new QuestionnaireException(e);
    	} catch (IOException e) {
    		throw new QuestionnaireException(Status.INTERNAL_SERVER_ERROR.getStatusCode(), e.getMessage());
		}
    }
    
    private String[] getHeaders(QuestionSet questionSet) {
    	final List<String> headers = new ArrayList<String>();
    	
    	headers.add("pilotId");
    	headers.add("app");
    	headers.add("lang");
    	headers.add("timestamp");
    	
    	for (final Question question : questionSet.getQuestions()) {
    		headers.add(question.getText());
    	}
    	
    	return headers.toArray(new String[headers.size()]);
    } 
    
    private Map<Integer, String> getResponseMap(ResponseSet responseSet) {
    	final Map<Integer, String> responseMap = new HashMap<Integer, String>();
    	
    	for (final eu.welive.bb.questionnaire.serialization.Response response : responseSet.getResponses()) {
    		responseMap.put(response.getQuestionId(), response.getResponse());
    	}
    	
    	return responseMap;
    }
    
    private Object[] getResponse(ResponseSet responseSet, QuestionSet questionSet) {
    	final List<String> responseList = new ArrayList<String>();
    	
    	responseList.add(responseSet.getPilotId());
    	responseList.add(responseSet.getApp());
    	responseList.add(responseSet.getLang().toString());
    	responseList.add(responseSet.getTimestamp());
    	
    	for (final Question question : questionSet.getQuestions()) {
    		final Map<Integer, String> responseMap = getResponseMap(responseSet);
    		responseList.add(responseMap.get(question.getId()));
    	}
    	
    	return responseList.toArray(new Object[responseList.size()]);
    }
    
    private String createCSV(Survey survey) throws IOException {
    	final StringWriter out = new StringWriter();
   	
    	final String[] headers = getHeaders(survey.getQuestionSet());
		try (final CSVPrinter printer = CSVFormat.DEFAULT.withQuote('|').withHeader(headers).print(out)) {
			
			for (final ResponseSet responseSet : survey.getResponseSets()) {
				printer.printRecord(getResponse(responseSet, survey.getQuestionSet()));
			}
			
			return out.toString();
		}
    }
    
    private ByteArrayOutputStream createZip(SurveySet surveySet) throws IOException {
    	final ByteArrayOutputStream baos = new ByteArrayOutputStream();
    	
    	int counter = 1;
    	try(ZipOutputStream zos = new ZipOutputStream(baos)) {
	    	for (final Survey survey : surveySet) {				
				final ZipEntry entry = new ZipEntry("survey_" + counter + ".csv");
				
				final String csvData = createCSV(survey);
				
				zos.putNextEntry(entry);
				zos.write(csvData.getBytes());
				zos.closeEntry();
				
				counter ++;
			}
    	}
    	
    	return baos;
    }
}
/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.api;

import eu.welive.bb.questionnaire.data.QWrapperException;

@SuppressWarnings("serial")
public class QuestionnaireException extends Exception {

	private final int status; 
	
	public QuestionnaireException(int status, String message) {
		super(message);
		this.status = status;
	}
	
	public QuestionnaireException(QWrapperException e) {
		super(e.getMessage() + " " + e.getReason());
		this.status = e.getStatus();
	} 
	
	public int getStatus() {
		return status;
	}
}

/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.swagger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import eu.welive.bb.questionnaire.config.Config;
import io.swagger.jaxrs.config.BeanConfig;

@WebListener
public class SwaggerInitializer implements ServletContextListener {

	public void contextInitialized(ServletContextEvent servletContextEvent) {
		BeanConfig beanConfig = new BeanConfig();
		beanConfig.setTitle("BB In-App API");
		beanConfig.setVersion("1.0.0");
		beanConfig.setResourcePackage("eu.welive.bb.questionnaire.api");
		
		beanConfig.setBasePath(Config.getInstance().getSwaggerPath());
		beanConfig.setHost(Config.getInstance().getHost());
	   
		beanConfig.setScan(true);			
	}

	public void contextDestroyed(ServletContextEvent servletContextEvent) {

	}
}
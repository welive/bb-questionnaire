/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.data;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Matchers;
import org.mockito.Mockito;

import eu.welive.bb.questionnaire.data.qm.QMConnector;
import eu.welive.bb.questionnaire.data.qm.QMConnectorException;
import eu.welive.bb.questionnaire.serialization.Question.Language;
import eu.welive.bb.questionnaire.serialization.Question.QuestionType;
import eu.welive.bb.questionnaire.serialization.QuestionSet;
import eu.welive.bb.questionnaire.serialization.Response;
import eu.welive.bb.questionnaire.serialization.ResponseSet;
import eu.welive.bb.questionnaire.serialization.survey.SurveySet;

public class QWrapperTest {

	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	private static final String RESOURCE_ID = "e5afb5f4-bfed-4976-b61a-b64647c48e10";
	private static final String PACKAGE_NAME = "sample-package";
	
	private static final String TEST_APP = "Test-app";
	private static final String TEST_API = "http://test.com/test";
	
	private static final String TOKEN = "XXXX-XXXX-XXXX-XXXX-XXXX";
	
	private QMConnector qmConnector;
	private QWrapper qWrapper;
	
	private JsonObject createQuestionSetResponse() {
		final JsonObject response = Json.createObjectBuilder()
			 .add("count", 3)		     
			 .add("rows", Json.createArrayBuilder()
				 .add(Json.createObjectBuilder()
					 .add("questionset_id", 100)
					 .add("title", "Test question set")
					 .add("description", "Some description")
					 .add("app", "app-name")
					 .add("id", 1)
					 .add("order", 1)
					 .add("timestamp", "2015/04/29 11:43:00")
					 .add("text", "Rate this app")
					 .add("lang", Language.EN.toString())
					 .add("type", QuestionType.VALUE_SET.toString())
					 .add("allowed", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]")
					 .add("labels", "['Poor', '', '', '', '', '', '', '', '', 'Excellent']")
				 )
				 .add(Json.createObjectBuilder()
					 .add("questionset_id", 100)
					 .add("title", "Test question set")
					 .add("description", "Some description")
					 .add("app", "app-name")
					 .add("id", 2)
					 .add("order", 2)
					 .add("timestamp", "2015/04/29 11:43:00")
					 .add("text", "Any comment?")
					 .add("lang", Language.EN.toString())
					 .add("type", QuestionType.FREE_TEXT.toString())
					 .add("allowed", "")
					 .add("labels", "")
				 )
				 .add(Json.createObjectBuilder()
					 .add("questionset_id", 100)
					 .add("title", "Test question set")
					 .add("description", "Some description")
					 .add("app", "app-name")
					 .add("id", 3)
					 .add("order", 3)
					 .add("timestamp", "2015/04/29 11:43:00")
					 .add("text", "Do you want to pay?")
					 .add("lang", Language.EN.toString())
					 .add("type", QuestionType.VALUE_SET.toString())
					 .add("allowed", "[0, 1]")
					 .add("labels", "['No', 'Yes']")
				)
			 )
		.build();
			 
		return response;
	}
	
	private JsonObject createQuestionSetResponseLabelsMissing() {
		final JsonObject response = Json.createObjectBuilder()
			 .add("count", 3)		     
			 .add("rows", Json.createArrayBuilder()
				 .add(Json.createObjectBuilder()
					 .add("questionset_id", 100)
					 .add("title", "Test question set")
					 .add("description", "Some description")
					 .add("app", "app-name")
					 .add("id", 1)
					 .add("order", 1)
					 .add("timestamp", "2015/04/29 11:43:00")
					 .add("text", "Rate this app")
					 .add("lang", Language.EN.toString())
					 .add("type", QuestionType.VALUE_SET.toString())
					 .add("allowed", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]")
				 )
				 .add(Json.createObjectBuilder()
					 .add("questionset_id", 100)
					 .add("title", "Test question set")
					 .add("description", "Some description")
					 .add("app", "app-name")
					 .add("id", 2)
					 .add("order", 2)
					 .add("timestamp", "2015/04/29 11:43:00")
					 .add("text", "Any comment?")
					 .add("lang", Language.EN.toString())
					 .add("type", QuestionType.FREE_TEXT.toString())
					 .add("allowed", "")
				 )
				 .add(Json.createObjectBuilder()
					 .add("questionset_id", 100)
					 .add("title", "Test question set")
					 .add("description", "Some description")
					 .add("app", "app-name")
					 .add("id", 3)
					 .add("order", 3)
					 .add("timestamp", "2015/04/29 11:43:00")
					 .add("text", "Do you want to pay?")
					 .add("lang", Language.EN.toString())
					 .add("type", QuestionType.VALUE_SET.toString())
					 .add("allowed", "[0, 1]")
				)
			 )
		.build();
			 
		return response;
	}
	
	private JsonObject createEmptyResponse() {
		final JsonObject response = Json.createObjectBuilder()
				.add("count", 0)		     
			.build();
				 
			return response;
	}
	
	private JsonObject createRowsMissing() {
		final JsonObject response = Json.createObjectBuilder()
				.add("count", 3)		     
			.build();
				 
			return response;
	}
	
	private JsonObject createResponseSetResponse(String app) {
		final JsonObject response = Json.createObjectBuilder()
			 .add("count", 3)		     
			 .add("rows", Json.createArrayBuilder()
				 .add(Json.createObjectBuilder()
					 .add("responseset_id", 1)
					 .add("questionset_id", 1)
					 .add("app", app)
					 .add("email", "a@a.com")
					 .add("lang", Language.EN.toString())
					 .add("country", "Bilbao")
					 .add("responseset_timestamp", "2015/04/29 11:43:00")
					 .add("value", "5")
					 .add("question_id", 1)
				 )
				 .add(Json.createObjectBuilder()
					 .add("responseset_id", 1)
					 .add("questionset_id", 1)
					 .add("app", app)
					 .add("email", "a@a.com")
					 .add("lang", Language.EN.toString())
					 .add("country", "Bilbao")
					 .add("responseset_timestamp", "2015/04/29 11:43:00")
					 .add("value", "Some long text introduced by the user")
					 .add("question_id", 2)
					 )
				 .add(Json.createObjectBuilder()
					 .add("responseset_id", 1)
					 .add("questionset_id", 1)
					 .add("app", app)
					 .add("email", "a@a.com")
					 .add("lang", Language.EN.toString())
					 .add("country", "Bilbao")
					 .add("responseset_timestamp", "2015/04/29 11:43:00")
					 .add("value", "1")
					 .add("question_id", 3)
				 )
			 )
		.build();
			 
		return response;
	}
	
	private void assertResponseSet(ResponseSet responseSet, String app) {
		assertEquals(3, responseSet.getResponses().size());
		
		assertEquals(1, responseSet.getId());
		assertEquals(1, responseSet.getQuestionSetId());
		assertEquals(app, responseSet.getApp());
		assertEquals("a@a.com", responseSet.getEmail());
		assertEquals(Language.EN, responseSet.getLang());
		assertEquals("Bilbao", responseSet.getPilotId());
		assertEquals("2015/04/29 11:43:00", responseSet.getTimestamp());
		
		assertEquals(1, responseSet.getResponses().get(0).getQuestionId());
		assertEquals("5", responseSet.getResponses().get(0).getResponse());
		
		assertEquals(2, responseSet.getResponses().get(1).getQuestionId());
		assertEquals("Some long text introduced by the user", responseSet.getResponses().get(1).getResponse());
		
		assertEquals(3, responseSet.getResponses().get(2).getQuestionId());
		assertEquals("1", responseSet.getResponses().get(2).getResponse());
	}
	
	private ResponseSet createResponseSet() {
		final ResponseSet responseSet = new ResponseSet();
		responseSet.setQuestionSetId(200);
		responseSet.setApp(TEST_APP);
		responseSet.setLang(Language.EN);
		responseSet.setPilotId("Bilbao");
		responseSet.setEmail("a@a.com");
		
		final Response response1 = new Response();
		response1.setQuestionId(1);
		response1.setResponse("5");
		responseSet.addResponse(response1);

		final Response response2 = new Response();
		response2.setQuestionId(2);
		response2.setResponse("In my opinion...");
		responseSet.addResponse(response2);

		final Response response3 = new Response();
		response3.setQuestionId(3);
		response3.setResponse("1");
		responseSet.addResponse(response3);
		
		return responseSet;
	}
	
	private ResponseSet createResponseSetInvalidQuestionId() {
		final ResponseSet responseSet = new ResponseSet();
		responseSet.setQuestionSetId(200);
		responseSet.setApp(TEST_APP);
		responseSet.setLang(Language.EN);
		responseSet.setPilotId("Bilbao");
		responseSet.setEmail("a@a.com");
		
		final Response response1 = new Response();
		response1.setQuestionId(10);
		response1.setResponse("5");
		responseSet.addResponse(response1);

		final Response response2 = new Response();
		response2.setQuestionId(2);
		response2.setResponse("In my opinion...");
		responseSet.addResponse(response2);

		final Response response3 = new Response();
		response3.setQuestionId(3);
		response3.setResponse("1");
		responseSet.addResponse(response3);
		
		return responseSet;
	}
	
	private ResponseSet createResponseSetMissingQuestions() {
		final ResponseSet responseSet = new ResponseSet();
		responseSet.setQuestionSetId(200);
		responseSet.setApp(TEST_APP);
		responseSet.setLang(Language.EN);
		responseSet.setPilotId("Bilbao");
		responseSet.setEmail("a@a.com");
		
		final Response response2 = new Response();
		response2.setQuestionId(2);
		response2.setResponse("In my opinion...");
		responseSet.addResponse(response2);
		
		return responseSet;
	}
	
	private ResponseSet createResponseSetMissingQSId() {
		final ResponseSet responseSet = new ResponseSet();
		responseSet.setLang(Language.EN);
		responseSet.setPilotId("Bilbao");
		responseSet.setEmail("a@a.com");
		
		return responseSet;
	}
	
	private ResponseSet createResponseSetMissingLang() {
		final ResponseSet responseSet = new ResponseSet();
		responseSet.setQuestionSetId(100);
		responseSet.setPilotId("Bilbao");
		responseSet.setEmail("a@a.com");
		
		return responseSet;
	}
	
	private ResponseSet createResponseSetMissingCountry() {
		final ResponseSet responseSet = new ResponseSet();
		responseSet.setQuestionSetId(100);
		responseSet.setLang(Language.EN);
		responseSet.setEmail("a@a.com");
		
		return responseSet;
	}
	
	private JsonObject createInvalidJson() {
		final JsonObject response = Json.createObjectBuilder().build();
		return response;
	}
	
	@Before
	public void setUp() throws QMConnectorException, QWrapperException {
		qmConnector = Mockito.mock(QMConnector.class);
		when(qmConnector.getAPI()).thenReturn(""); 
		
		qWrapper = new QWrapper(PACKAGE_NAME, RESOURCE_ID, qmConnector, TOKEN);
	}

	@Test
	public void testGetQuestionSet() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse());
		
		final QuestionSet questionSet = qWrapper.getQuestionSet(1, Language.EN);
		
		assertQuestionSet(questionSet);
	}

	private void assertQuestionSet(final QuestionSet questionSet) {
		assertEquals(100, questionSet.getId());
		assertEquals("Test question set", questionSet.getTitle());
		assertEquals("Some description", questionSet.getDescription());
		assertEquals("app-name", questionSet.getApp());
		assertEquals(3, questionSet.getQuestions().size());
		assertEquals("2015/04/29 11:43:00", questionSet.getTimestamp());
		
		assertEquals(1, questionSet.getQuestions().get(0).getId());
		assertEquals(1, questionSet.getQuestions().get(0).getOrder());
		assertEquals(Language.EN, questionSet.getQuestions().get(0).getLang());
		assertEquals("Rate this app", questionSet.getQuestions().get(0).getText());
		assertEquals(QuestionType.VALUE_SET, questionSet.getQuestions().get(0).getType());
		assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]", questionSet.getQuestions().get(0).getAllowedResponse());
		assertEquals("['Poor', '', '', '', '', '', '', '', '', 'Excellent']", questionSet.getQuestions().get(0).getResponseLabels());
		
		assertEquals(2, questionSet.getQuestions().get(1).getId());
		assertEquals(2, questionSet.getQuestions().get(1).getOrder());
		assertEquals(Language.EN, questionSet.getQuestions().get(1).getLang());
		assertEquals("Any comment?", questionSet.getQuestions().get(1).getText());
		assertEquals(QuestionType.FREE_TEXT, questionSet.getQuestions().get(1).getType());
		assertTrue(questionSet.getQuestions().get(1).getAllowedResponse().isEmpty());
		assertTrue(questionSet.getQuestions().get(1).getResponseLabels().isEmpty());
		
		assertEquals(3, questionSet.getQuestions().get(2).getId());
		assertEquals(3, questionSet.getQuestions().get(2).getOrder());
		assertEquals(Language.EN, questionSet.getQuestions().get(2).getLang());
		assertEquals("Do you want to pay?", questionSet.getQuestions().get(2).getText());
		assertEquals(QuestionType.VALUE_SET, questionSet.getQuestions().get(2).getType());
		assertEquals("[0, 1]", questionSet.getQuestions().get(2).getAllowedResponse());
		assertEquals("['No', 'Yes']", questionSet.getQuestions().get(2).getResponseLabels());
	}
	
	@Test
	public void testGetQuestionSetLabelsMissing() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponseLabelsMissing());
		
		final QuestionSet questionSet = qWrapper.getQuestionSet(1, Language.EN);
		
		assertQuestionSetWithoutLabels(questionSet);
	}

	private void assertQuestionSetWithoutLabels(final QuestionSet questionSet) {
		assertEquals(100, questionSet.getId());
		assertEquals("Test question set", questionSet.getTitle());
		assertEquals("Some description", questionSet.getDescription());
		assertEquals("app-name", questionSet.getApp());
		assertEquals(3, questionSet.getQuestions().size()); 
		
		assertEquals(100, questionSet.getId());
		assertEquals("Test question set", questionSet.getTitle());
		assertEquals(3, questionSet.getQuestions().size()); 
		
		assertEquals(1, questionSet.getQuestions().get(0).getId());
		assertEquals(1, questionSet.getQuestions().get(0).getOrder());
		assertEquals(Language.EN, questionSet.getQuestions().get(0).getLang());
		assertEquals("Rate this app", questionSet.getQuestions().get(0).getText());
		assertEquals(QuestionType.VALUE_SET, questionSet.getQuestions().get(0).getType());
		assertEquals("[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]", questionSet.getQuestions().get(0).getAllowedResponse());
		assertTrue(questionSet.getQuestions().get(0).getResponseLabels().isEmpty());
		
		assertEquals(2, questionSet.getQuestions().get(1).getId());
		assertEquals(2, questionSet.getQuestions().get(1).getOrder());
		assertEquals(Language.EN, questionSet.getQuestions().get(1).getLang());
		assertEquals("Any comment?", questionSet.getQuestions().get(1).getText());
		assertEquals(QuestionType.FREE_TEXT, questionSet.getQuestions().get(1).getType());
		assertTrue(questionSet.getQuestions().get(1).getAllowedResponse().isEmpty());
		assertTrue(questionSet.getQuestions().get(1).getResponseLabels().isEmpty());
		
		assertEquals(3, questionSet.getQuestions().get(2).getId());
		assertEquals(3, questionSet.getQuestions().get(2).getOrder());
		assertEquals(Language.EN, questionSet.getQuestions().get(2).getLang());
		assertEquals("Do you want to pay?", questionSet.getQuestions().get(2).getText());
		assertEquals(QuestionType.VALUE_SET, questionSet.getQuestions().get(2).getType());
		assertEquals("[0, 1]", questionSet.getQuestions().get(2).getAllowedResponse());
		assertTrue(questionSet.getQuestions().get(2).getResponseLabels().isEmpty());
	}
	
	@Test
	public void testGetQuestionSetRowsMissing() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("Invalid JSON received. 'rows' field not found");
		
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createRowsMissing());
		
		qWrapper.getQuestionSet(2, Language.EN); 
	}
	
	@Test
	public void testGetQuestionSetIdNotFound() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("QuestionSet '2' not found");
		
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createEmptyResponse());
		
		qWrapper.getQuestionSet(2, Language.EN); 
	}
	
	@Test
	public void testGetQuestionSetLatestNotFound() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("QuestionSet not found");
		
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createEmptyResponse());
		
		qWrapper.getQuestionSet(QWrapper.LATEST_QUESTIONSET, Language.EN); 
	}
	
	@Test(expected=QWrapperException.class)
	public void testGestQuestionQueryError() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenThrow(new QMConnectorException(""));
		
		qWrapper.getQuestionSet(1, Language.EN);
	}
		
	@Test
	public void testGetQuestionSetInvalidJson() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("Invalid JSON received. 'count' field not found");
		
		when(qmConnector.queryDataset(Matchers.anyString(), Matchers.anyString(), Matchers.anyString()))
			.thenReturn(createInvalidJson());
		
		qWrapper.getQuestionSet(1, Language.EN);
	}
	
	@Test
	public void testGetLastQuestionSet() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse());
		
		final QuestionSet questionSet = qWrapper.getQuestionSet(QWrapper.LATEST_QUESTIONSET, Language.EN);
		
		assertQuestionSet(questionSet);
	} 
	
	@Test(expected=QWrapperException.class)
	public void testGetLastQuestionSetQueryError() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenThrow(new QMConnectorException(""));
		
		qWrapper.getQuestionSet(QWrapper.LATEST_QUESTIONSET, Language.EN);
	}
	
	private JsonObject createResponseSetIdResponse() {
		final JsonObject response = Json.createObjectBuilder()
			 .add("count", 1)		     
			 .add("rows", Json.createArrayBuilder()
				 .add(Json.createObjectBuilder()
					 .add("responseSetId", 3)
				 )
			 )
		.build();
			 
		return response;
	} 
	
	@Test
	public void testAddResponseSet() throws QMConnectorException, QWrapperException {
		final ResponseSet responseSet = createResponseSet();
		final int insertedRows = responseSet.getResponses().size() + 1;
		
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse());
		
		when(qmConnector.queryDataset(Matchers.eq("SELECT MAX(id) as responseSetId FROM ResponseSet"), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createResponseSetIdResponse());
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(insertedRows);
		
		when(qmConnector.createTimestamp())
			.thenReturn("2016/04/29 16:9:27");
		
		when(qmConnector.getAPI())
			.thenReturn(TEST_API);
		
		final int updatedRows = qWrapper.addResponseSet(responseSet);
		
		assertEquals(insertedRows, updatedRows);
		
		final List<String> inserts = new ArrayList<String>();
		inserts.add("INSERT INTO ResponseSet (id, questionset_id, app, lang, country, timestamp, email) VALUES (null, 200, \"Test-app\", \"EN\", \"Bilbao\", \"2016/04/29 16:9:27\", \"a@a.com\")");
		
		inserts.add("INSERT INTO replies (id, value, responseset_id, question_id) SELECT null, '5', MAX(id), 1 FROM ResponseSet");
		inserts.add("INSERT INTO replies (id, value, responseset_id, question_id) SELECT null, 'In my opinion...', MAX(id), 2 FROM ResponseSet");
		inserts.add("INSERT INTO replies (id, value, responseset_id, question_id) SELECT null, '1', MAX(id), 3 FROM ResponseSet");
		
		verify(qmConnector).updateDatasetTransactional(Matchers.eq(inserts), Matchers.anyString(), Matchers.anyString());
	}
	
	@Test(expected=QWrapperException.class)
	public void testAddResponseSetUpdateDatasetError() throws QMConnectorException, QWrapperException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse());
		
		when(qmConnector.queryDataset(Matchers.eq("SELECT MAX(id) as responseSetId FROM ResponseSet"), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createResponseSetIdResponse());
		
		when(qmConnector.updateDatasetTransactional(Matchers.anyListOf(String.class), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenThrow(new QMConnectorException(""));
		
		final ResponseSet responseSet = createResponseSet();
		
		qWrapper.addResponseSet(responseSet);
	}
	
	@Test
	public void testAddResponseSetMissingQSId() throws QWrapperException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("ResponseSet has no 'questionSetId'");
		
		final ResponseSet responseSet = createResponseSetMissingQSId();
		qWrapper.addResponseSet(responseSet);
	}
	
	@Test
	public void testAddResponseSetMissingLang() throws QWrapperException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("ResponseSet has no 'lang'");
		
		final ResponseSet responseSet = createResponseSetMissingLang();
		qWrapper.addResponseSet(responseSet);
	}
	
	@Test
	public void testAddResponseSetMissingCountry() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("ResponseSet has no 'pilotID'");
	    
	    when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse());
		
		final ResponseSet responseSet = createResponseSetMissingCountry();
		qWrapper.addResponseSet(responseSet);
	}
	
	@Test
	public void testAddResponseSetInvalidQuestionId() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("Question '10' not found in QuestionSet '100");
	    
	    when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse());
		
		final ResponseSet responseSet = createResponseSetInvalidQuestionId();
		qWrapper.addResponseSet(responseSet);
	}
	
	@Test
	public void testAddResponseSetMissingQuestions() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("Some questions of QuestionSet '100' were not answered: [1, 3]");
	    
	    when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse());
		
		final ResponseSet responseSet = createResponseSetMissingQuestions();
		qWrapper.addResponseSet(responseSet);
	}
	
	@Test
	public void testGetQuestionSets() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse());
		
		final List<QuestionSet> questionSets = qWrapper.getQuestionSets();
		
		assertEquals(1, questionSets.size());
	}
	
	@Test
	public void testGetQuestionSetsEmpty() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createEmptyResponse());
		
		final List<QuestionSet> questionSets = qWrapper.getQuestionSets();
		
		assertTrue(questionSets.isEmpty());
	}
	
	@Test(expected=QWrapperException.class)
	public void testGetQuestionSetsInvalidJson() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createInvalidJson());
		
		final List<QuestionSet> questionSets = qWrapper.getQuestionSets();
		
		assertTrue(questionSets.isEmpty());
	}
	
	@Test
	public void testGetSurveySet() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse())
			.thenReturn(createResponseSetResponse("App1"));
		
		final SurveySet surveySet = qWrapper.getSurveySet("", "");
		
		assertEquals(1, surveySet.getSurveys().size());
		assertNotNull(surveySet.getSurveys().get(0).getQuestionSet());
		assertEquals(1, surveySet.getSurveys().get(0).getResponseSets().size());
		
		assertResponseSet(surveySet.getSurveys().get(0).getResponseSets().get(0), "App1");
	}
	
	@Test
	public void testGetSurveySetApp() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse())
			.thenReturn(createResponseSetResponse("App2"));
		
		final SurveySet surveySet = qWrapper.getSurveySet("", "App2");
		
		assertEquals(1, surveySet.getSurveys().size());
		assertNotNull(surveySet.getSurveys().get(0).getQuestionSet());
		assertEquals(1, surveySet.getSurveys().get(0).getResponseSets().size());
		
		assertResponseSet(surveySet.getSurveys().get(0).getResponseSets().get(0), "App2");
	}
	
	@Test
	public void testGetSurveySetRowsMissingQuestionSet() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("Invalid JSON received. 'rows' field not found");
		
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createRowsMissing());
		
		qWrapper.getSurveySet("", "");
	}
	
	@Test
	public void testGetSurveySetRowsMissingResponseSet() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("Invalid JSON received. 'rows' field not found");
		
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse())
			.thenReturn(createRowsMissing());
		
		qWrapper.getSurveySet("", "");
	}
	
	@Test
	public void testGetSurveySetLabelsMissing() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponseLabelsMissing())
			.thenReturn(createResponseSetResponse("App1"));
		
		final SurveySet surveySet = qWrapper.getSurveySet("", "App1");
		
		assertEquals(1, surveySet.getSurveys().size());
		assertNotNull(surveySet.getSurveys().get(0).getQuestionSet());
		assertEquals(1, surveySet.getSurveys().get(0).getResponseSets().size());
	}
	
	@Test
	public void testGetSurveyInvalidJsonResponseSet() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("Invalid JSON received. 'count' field not found");
				
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse())
			.thenReturn(createInvalidJson());
		
		qWrapper.getSurveySet("", "");
	}
	
	@Test
	public void testGetSurveyInvalidJsonResponseSetApp() throws QWrapperException, QMConnectorException {
		exception.expect(QWrapperException.class);
	    exception.expectMessage("Invalid JSON received. 'count' field not found");
				
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse())
			.thenReturn(createInvalidJson());
		
		qWrapper.getSurveySet("", "App2");
	}
	
	@Test
	public void testGetSurveyEmptyResponseSet() throws QWrapperException, QMConnectorException {
		when(qmConnector.queryDataset(Matchers.anyString(), 
				Matchers.eq(PACKAGE_NAME), Matchers.eq(RESOURCE_ID)))
			.thenReturn(createQuestionSetResponse())
			.thenReturn(createEmptyResponse());
		
		final SurveySet surveySet = qWrapper.getSurveySet("", "App1");
		
		assertEquals(1, surveySet.getSurveys().size());
		assertNotNull(surveySet.getSurveys().get(0).getQuestionSet());
		assertTrue(surveySet.getSurveys().get(0).getResponseSets().isEmpty());
	}
}
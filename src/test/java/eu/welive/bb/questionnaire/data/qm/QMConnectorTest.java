/*
Copyright 2015-2018 University of Deusto

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/


package eu.welive.bb.questionnaire.data.qm;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonObject;
import javax.ws.rs.ProcessingException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation.Builder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Matchers;
import org.mockito.Mockito;

import eu.welive.bb.questionnaire.serialization.Question.Language;
import eu.welive.bb.questionnaire.serialization.Question.QuestionType;

public class QMConnectorTest {
	
	@Rule
	public ExpectedException exception = ExpectedException.none();
	
	private static final String DATASET = "dataset-name";
	private static final String RESOURCE = "resource-id";
	
	private WebTarget target; 
	private Builder builder;
	private QMConnector qmConnector; 
	
	private JsonObject createJsonData() {
		final JsonObject response = Json.createObjectBuilder()
			 .add("count", 3)		     
			 .add("rows", Json.createArrayBuilder()
				 .add(Json.createObjectBuilder()
					 .add("questionset_id", 100)
					 .add("title", "Test question set")
					 .add("id", 1)
					 .add("order", 1)
					 .add("timestamp", "2015/04/29 11:43:00")
					 .add("text", "Rate this app")
					 .add("lang", Language.EN.toString())
					 .add("type", QuestionType.VALUE_SET.toString())
					 .add("allowed", "[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]")
					 .add("labels", "['Poor', '', '', '', '', '', '', '', '', 'Excellent']")
				 )
				 .add(Json.createObjectBuilder()
					 .add("questionset_id", 100)
					 .add("title", "Test question set")
					 .add("id", 2)
					 .add("order", 2)
					 .add("timestamp", "2015/04/29 11:43:00")
					 .add("text", "Any comment?")
					 .add("lang", Language.EN.toString())
					 .add("type", QuestionType.FREE_TEXT.toString())
					 .add("allowed", "")
					 .add("labels", "")
				 )
				 .add(Json.createObjectBuilder()
					 .add("questionset_id", 100)
					 .add("title", "Test question set")
					 .add("id", 3)
					 .add("order", 3)
					 .add("timestamp", "2015/04/29 11:43:00")
					 .add("text", "Do you want to pay?")
					 .add("lang", Language.EN.toString())
					 .add("type", QuestionType.VALUE_SET.toString())
					 .add("allowed", "[0, 1]")
					 .add("labels", "['No', 'Yes']")
				)
			 )
		.build();
				 
		return response;
	}
	
	private JsonObject createJsonError(String message, String reason) {
		final JsonObject response = Json.createObjectBuilder()
			 .add("error", true) 
			 .add("message", message)
			 .add("reason", reason)
			 .add("status", Status.INTERNAL_SERVER_ERROR.getStatusCode())
		.build();
				 
		return response;
	}
	
	private JsonObject createJsonRowsUpdated(int rows) {
		final JsonObject response = Json.createObjectBuilder()
			 .add("rows", rows) 
		.build();
				 
		return response;
	}
	
	private JsonObject createJsonRowsMissing() {
		final JsonObject response = Json.createObjectBuilder() 
		.build();
				 
		return response;
	}
	
	@Before
	public void setUp() {
		target = Mockito.mock(WebTarget.class);
		builder = Mockito.mock(Builder.class);
		
		when(target.path(Matchers.anyString()))
			.thenReturn(target);
		
		when(target.queryParam(Matchers.anyString(), Matchers.anyString()))
			.thenReturn(target);
	
		when(target.request(Matchers.eq(MediaType.APPLICATION_JSON)))
			.thenReturn(builder);
		
		when(builder.header(Matchers.anyString(), Matchers.anyString()))
			.thenReturn(builder);
		
		qmConnector = new QMConnector(target);
	}
	
	@SuppressWarnings("unchecked")
	private Response createResponseOk(JsonObject data) {
		final Response response = Mockito.mock(Response.class);
		
		when(response.getStatus())
			.thenReturn(Status.OK.getStatusCode());
		
		when(response.readEntity(Matchers.any(Class.class)))
			.thenReturn(data.toString());
		
		return response;
	}
	
	@SuppressWarnings("unchecked")
	private Response createResponseError(String message, String reason) {
		final Response response = Mockito.mock(Response.class);
		
		when(response.getStatus())
			.thenReturn(Status.INTERNAL_SERVER_ERROR.getStatusCode());
		
		when(response.readEntity(Matchers.any(Class.class)))
			.thenReturn(createJsonError(message, reason).toString());
		
		return response;
	}

	@Test
	public void testQueryDataset() throws QMConnectorException {
		final Response response = createResponseOk(createJsonData());
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenReturn(response);
		
		final JsonObject data = qmConnector.queryDataset("SELECT * FROM table;", DATASET, RESOURCE);
		
		assertEquals(createJsonData(), data);
	}
	
	@Test
	public void testQueryDatasetError() throws QMConnectorException {
		exception.expect(QMConnectorException.class);
	    exception.expectMessage(String.format("Problem executing query on dataset '%s' resource '%s'.", DATASET, RESOURCE));
		
		final Response errorResponse = createResponseError("Some message", "Some reason");
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenReturn(errorResponse);
		
		qmConnector.queryDataset("SELECT * FROM table;", DATASET, RESOURCE);
	}
	
	@Test
	public void testQueryDatasetProcessingException() throws QMConnectorException {
		exception.expect(QMConnectorException.class);
		exception.expectMessage(String.format("Problem executing query on dataset '%s' resource '%s'.", DATASET, RESOURCE));
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenThrow(new ProcessingException("Some unexpected problem"));
		
		qmConnector.queryDataset("SELECT * FROM table;", DATASET, RESOURCE);
	}
	
	@Test
	public void testQueryDatasetAuthHeader() throws QMConnectorException {
		final Response response = createResponseOk(createJsonData());
		
		final String token = "Bearer XXXX-XXXX-XXXX-XXXX";
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenReturn(response);
		
		qmConnector.setAuthHeader(token);
		final JsonObject data = qmConnector.queryDataset("SELECT * FROM table;", DATASET, RESOURCE);
		
		verify(builder).header(Matchers.eq(QMConnector.AUTH_HEADER), Matchers.eq(token));
		
		assertEquals(createJsonData(), data);
	} 

	@Test
	public void testUpdateDataset() throws QMConnectorException {
		final Response response = createResponseOk(createJsonRowsUpdated(3));
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenReturn(response);
		
		final int updatedRows = qmConnector.updateDataset("INSERT INTO table (column1, column2, column3) VALUES (null, 1, 'a')", DATASET, RESOURCE);
		
		assertEquals(3, updatedRows);
	}
	
	@Test
	public void testUpdateDatasetResponseError() throws QMConnectorException {
		exception.expect(QMConnectorException.class);
		exception.expectMessage(String.format("Problem executing update on dataset '%s' resource '%s'. Some unexpected problem", DATASET, RESOURCE));
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenThrow(new ProcessingException("Some unexpected problem"));
		
		qmConnector.updateDataset("INSERT INTO table (column1, column2, column3) VALUES (null, 1, 'a')", DATASET, RESOURCE);
	}
	
	@Test
	public void testUpdateDatasetResponseRowsMissing() throws QMConnectorException {
		exception.expect(QMConnectorException.class);
		exception.expectMessage(String.format("Problem executing update on dataset '%s' resource '%s'. 'rows' field not found in response", DATASET, RESOURCE));
		
		final Response response = createResponseOk(createJsonRowsMissing());
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenReturn(response);
		
		qmConnector.updateDataset("INSERT INTO table (column1, column2, column3) VALUES (null, 1, 'a')", DATASET, RESOURCE);
	}

	@Test
	public void testUpdateDatasetProcessingError() throws QMConnectorException {
		exception.expect(QMConnectorException.class);
		exception.expectMessage(String.format("Problem executing update on dataset '%s' resource '%s'.", DATASET, RESOURCE));
		
		final Response errorResponse = createResponseError("Some message", "Some reason");
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenReturn(errorResponse);
		
		qmConnector.updateDataset("INSERT INTO table (column1, column2, column3) VALUES (null, 1, 'a')", DATASET, RESOURCE);
	}
	
	@Test
	public void testUpdateDatasetTransactional() throws QMConnectorException {
		final Response response = createResponseOk(createJsonRowsUpdated(3));
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenReturn(response);
		
		final List<String> updates = new ArrayList<String>();
		updates.add("INSERT INTO table (column1, column2, column3) VALUES (null, 1, 'a')");
		updates.add("INSERT INTO table (column1, column2, column3) VALUES (null, 1, 'a')");
		
		final int updatedRows = qmConnector.updateDatasetTransactional(updates, DATASET, RESOURCE);
		
		assertEquals(3, updatedRows);
		
		verify(target).queryParam(Matchers.eq("transaction"), Matchers.eq("true"));
	}
	
	@Test
	public void testUpdateDatasetTranscationalProcessingError() throws QMConnectorException {
		exception.expect(QMConnectorException.class);
		exception.expectMessage(String.format("Problem executing update on dataset '%s' resource '%s'. Some unexpected problem", DATASET, RESOURCE));
		
		when(builder.post(Matchers.any(Entity.class)))
			.thenThrow(new ProcessingException("Some unexpected problem"));
		
		final List<String> updates = new ArrayList<String>();
		updates.add("INSERT INTO table (column1, column2, column3) VALUES (null, 1, 'a')");
		updates.add("INSERT INTO table (column1, column2, column3) VALUES (null, 1, 'a')");
		
		qmConnector.updateDatasetTransactional(updates, DATASET, RESOURCE);
	}
	
	@Test
	public void testGetApi() throws IOException {
		final String TEST_API = "http://test.com/test";
		final QMConnector qmConnector = new QMConnector(TEST_API);
		
		assertEquals(TEST_API, qmConnector.getAPI());
	}
	
	@Test
	public void testCreateTimestamp() throws ParseException {
		final String timestamp = qmConnector.createTimestamp();
		
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		dateFormat.parse(timestamp);
	}
}

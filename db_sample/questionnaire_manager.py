# -*- coding: utf-8 -*-

from datetime import datetime
#from data_wrapper_sqlite import update_dataset, query_dataset, update_dataset_transaction
from data_wrapper_qm import update_dataset, query_dataset, update_dataset_transaction
import sys
import os

def clear_screen():
    os.system('cls' if os.name == 'nt' else 'clear')

def add_questionset():
    clear_screen()
    print 'New QuestionSet'
    print '==============='

    title = raw_input('Title: ')
    current_date = datetime.now()
    timestamp = current_date.strftime('%Y-%M-%d %H:%M:%S')

    update_template = "INSERT INTO QuestionSet (id, title, timestamp) VALUES (null, '%s', '%s')"
    update = update_template % (title, timestamp)

    update_dataset(update)

def read_question_type():
    valid = False

    while not valid:
        question_type = raw_input("Question type ('FREE_TEXT', 'VALUE_SET', 'RANGE_SET'): ")
        if question_type in ['FREE_TEXT', 'VALUE_SET', 'RANGE_SET']:
            valid = True

    return question_type

def add_question(questionset_id):
    clear_screen()
    print 'New question in QuestionSet %d' % questionset_id
    print '=============================='
    print ''

    text = raw_input('Question text: ')
    lang = raw_input('Lang: ')
    order = int(raw_input('Order: '))

    print ''
    show_all_question_types()

    print 'Please, select a type'
    type_id = int(raw_input('Type ID: '))

    updates = []

    question_template = "INSERT INTO Question (id, 'order', type_id) VALUES (null, %d, %d)"
    update = question_template % (order, type_id)
    updates.append(update)

    contains_template = "INSERT INTO contains (id, questionset_id, question_id) SELECT null, %d, MAX(id) FROM Question"
    update = contains_template % questionset_id
    updates.append(update)

    qtrans_template = "INSERT INTO QTranslation (id, text, lang, question_id) SELECT null, '%s', '%s', MAX(id) FROM Question"
    update = qtrans_template % (text, lang)
    updates.append(update)

    update_dataset_transaction(updates)

def add_question_type():
    clear_screen()
    print 'New QuestionType'
    print '================'

    question_type = read_question_type()
    allowed = raw_input('Allowed values: ')

    insert_type_template = "INSERT INTO QuestionType (id, type, allowed) VALUES (null, '%s', '%s')"
    insert = insert_type_template % (question_type, allowed)

    update_dataset(insert)

def update_question_type(questiontype_id, question_type, allowed):
    clear_screen()
    print 'Updating QuestionType'
    print '====================='
    print ''

    update_template = """UPDATE QuestionType SET type='%s', allowed='%s'
                         WHERE id = %d"""

    update = update_template % (question_type, allowed, questiontype_id)

    update_dataset(update)

def show_all_question_types():
    clear_screen()
    print 'QuestionType List'
    print '================='
    print ''

    query = 'SELECT id, type, allowed FROM QuestionType'
    result = query_dataset(query)

    for row in result['rows']:
        print 'ID: %d' % row['id']
        print 'Type: %s' % row['type']
        print 'Allowed: %s' % row['allowed']
        print '------------'

def show_all_questionsets():
    clear_screen()
    print 'QuestionSet List'
    print '================'
    print ''

    query = 'SELECT id, title, timestamp FROM QuestionSet'

    result = query_dataset(query)

    if result['count'] > 0:
        for row in result['rows']:
            print 'ID: %d' % row['id']
            print 'Title: %s' % row['title']
            print 'Timestamp: %s' % row['timestamp']
            print '------------'
    else:
        print 'Empty list'

def get_questions(questionset_id, lang):
    query_template = """SELECT Question.id as id, text, QTranslation.lang, "order", type, allowed, labels
        FROM QuestionSet, contains, Question, QuestionType, QTranslation
        LEFT JOIN LTranslation ON (QuestionType.id = LTranslation.questiontype_id AND LTranslation.lang = '%s')
        WHERE QuestionSet.id = %d
            AND contains.questionset_id = QuestionSet.id
            AND contains.question_id = Question.id
            AND QuestionType.id = Question.type_id
            AND QTranslation.question_id = Question.id
            AND (QTranslation.lang = '%s' OR QTranslation.lang = 'EN')
        GROUP BY Question.id ORDER BY \"order\""""

    query = query_template % (lang, questionset_id, lang)
    results = query_dataset(query)

    return results

def update_question_translation(question_id, lang, text):
    update_template = """UPDATE QTranslation SET text = '%s'
                         WHERE question_id = %d AND lang = '%s'"""

    update = update_template % (text, question_id, lang)
    update_dataset(update)

def update_question(question_id, order, questiontype_id):
    update_template = """UPDATE Question SET \"order\"=%d, type_id=%d WHERE id = %d"""

    update = update_template % (order, questiontype_id, question_id)
    update_dataset(update)

def update_translations(questionset_id, lang):
    clear_screen()
    print 'Updating QTranslation'
    print '---------------------'
    print ''

    questions = get_questions(questionset_id, lang)
    if questions['count'] > 0:
        for row in questions['rows']:
            print 'Old text: %s' % row['text']
            new_text = raw_input('New text: ')
            if len(new_text) > 0:
                update_question_translation(row['id'], lang, new_text)
    else:
        print 'Empty list'

def show_questions(questionset_id, lang):
    clear_screen()
    print 'Question List'
    print '============='
    print ''

    results = get_questions(questionset_id, lang)

    if results['count'] > 0:
        for row in results['rows']:
            print 'ID: %d' % row['id']
            print 'Text: %s' % row['text']
            print 'Lang: %s' % row['lang']
            print 'Order: %d' % row['order']
            print 'Type: %s' % row['type']
            print 'Allowed: %s' % row['allowed']
            if 'labels' in row:
                print 'Labels: %s' % row['labels']
            print '------------'
    else:
        print 'Empty list'

def add_translation(question_id):
    clear_screen()
    print 'New QTranslation'
    print '================'

    lang = raw_input('Lang: ')
    text = raw_input('Text: ')

    qtrans_template = 'INSERT INTO QTranslation (id, text, lang, question_id) VALUES (null, "%s", "%s", %d)'
    update = qtrans_template % (text, lang, question_id)
    update_dataset(update)

def show_translations(question_id):
    clear_screen()
    print 'QTranslation List'
    print '================'
    print ''

    query = "SELECT text, lang FROM QTranslation WHERE question_id = %d" % question_id
    results = query_dataset(query)

    if results['count'] > 0:
        for row in results['rows']:
            print 'Lang: %s' % row['lang']
            print 'Text: %s' % row['text']
            print '------------'
    else:
        print 'Emtpy list'

def show_label_translations(questiontype_id):
    clear_screen()
    print 'LTranslation List'
    print '================'
    print ''

    query = "SELECT id, lang, labels FROM LTranslation WHERE questiontype_id = %d" % questiontype_id
    results = query_dataset(query)

    if results['count'] > 0:
        for row in results['rows']:
            print 'ID: %d' % row['id']
            print 'Lang: %s' % row['lang']
            print 'Labels: %s' % row['labels']
            print '------------'
    else:
        print 'Emtpy list'

def add_label_translation(questiontype_id):
    clear_screen()
    print 'New LTranslation'
    print '================'

    labels = raw_input('Labels: ')
    lang = raw_input('Lang: ')

    insert_template = "INSERT INTO LTranslation (id, lang, labels, questiontype_id) VALUES (null, '%s', '%s', %d)"
    insert = insert_template % (lang, labels, questiontype_id)

    update_dataset(insert)

if __name__ == '__main__':
    end = False
    while not end:
        clear_screen()
        print 'Questionnaire manager'
        print '---------------------'
        print ''

        print '1. Show question sets'
        print '2. Add question set'
        print '3. Show questions'
        print '4. Add question'
        print '5. Update question'
        print '6. Show question translations'
        print '7. Add question translation'
        print '8. Update questionset translations'
        print '9. Show question types'
        print '10. Add question type'
        print '11. Update question type'
        print '12. Show label translations'
        print '13. Add label translation'
        print '14. Exit'

        option = int(raw_input('Select an option: '))

        if option == 1:
            show_all_questionsets()
        elif option == 2:
            add_questionset()
        elif option == 3:
            questionset_id = int(raw_input('QuestionSet ID: '))
            lang = raw_input('Lang: ')
            show_questions(questionset_id, lang)
        elif option == 4:
            questionset_id = int(raw_input('QuestionSet ID: '))
            add_question(questionset_id)
        elif option == 5:
            question_id = int(raw_input('Question ID: '))
            order = int(raw_input('Order: '))
            questiontype_id = int(raw_input('QuestionType ID: '))
            update_question(question_id, order, questiontype_id)
        elif option == 6:
            question_id = int(raw_input('Question ID: '))
            show_translations(question_id)
        elif option == 7:
            question_id = int(raw_input('Question ID: '))
            add_translation(question_id)
        elif option == 8:
            questionset_id = int(raw_input('QuestionSet ID: '))
            lang = raw_input('Lang: ')
            update_translations(questionset_id, lang)
        elif option == 9:
            show_all_question_types()
        elif option == 10:
            add_question_type()
        elif option == 11:
            questiontype_id = int(raw_input("ID: "))
            question_type = raw_input("Type: ")
            allowed = raw_input("Allowed: ")
            update_question_type(questiontype_id, question_type, allowed)
        elif option == 12:
            questiontype_id = int(raw_input('QuestionType ID: '))
            show_label_translations(questiontype_id)
        elif option == 13:
            questiontype_id = int(raw_input('QuestionType ID: '))
            add_label_translation(questiontype_id)
        elif option == 14:
            sys.exit(0)

        raw_input('Press ENTER to continue...')

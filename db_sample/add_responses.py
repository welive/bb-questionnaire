import requests
import json

QUESTIONNAIRE_API = 'http://localhost:8090/in-app/api/questionnaire'

def add_response_set(response_set):
    add_response_set_url = QUESTIONNAIRE_API + '/response-set'
    r = requests.post(add_response_set_url,
        data=json.dumps(response_set),
        headers={'content-type': 'application/json'})

    if r.status_code == 200:
        print 'Response set saved'
    else:
        error = r.json()
        print error['message']


if __name__ == '__main__':
    response_set = {
        'questionSetId': 1,
        'app': 'Test-app',
        'lang': 'EN',
        'country': 'EN',
        'email': 'some@email.com',
        'responses': [
            {
                'questionId': 1,
                'response': '3'
            },
            {
                'questionId': 2,
                'response': '5'
            },
            {
                'questionId': 3,
                'response': '1'
            },
            {
                'questionId': 4,
                'response': '7'
            },
            {
                'questionId': 5,
                'response': 'No way I will pay!'
            }
        ]
    }

    add_response_set(response_set)
